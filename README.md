# Service Usage

* DKFZ *FileSender*

## Plotting

* Plotting is be performed in the [Plotting project](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci).

## Data + Weighing

./stats/usage-stats-filesender-weekly.csv - schedule: weekly

| Data | Unit | Comment | Weighting |
| ----- | ----- | ----- | ----- |
| user count | Quantity/Number | Nr of users | 12,5% |
| currently available transfers | Quantity/Number | Nr of available transfers | 12,5% |
| created transfers | Quantity/Number | Nr of created transfers | 12,5% |
| files uploaded | Quantity/Number | Nr of uploaded files | 12,5% |
| files downloaded | Quantity/Number | Nr of downloaded files | 12,5% |
| transfered up | Quantity/Number | Uploaded bytes | 12,5% |
| transfered down | Quantity/Number | Downloaded bytes | 12,5% |
| used space | Quantity/Number | Used space in bytes | 12,5% |
| xxxx | Quantity/Number| Nr of total xxxx | xx% |
| xxxx | Quantity/Number| Nr, will be considered later | 0% |

## Schedule

* weekly
