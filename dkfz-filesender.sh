#!/bin/bash

DB_NAME=$FILESENDER_DB_NAME
DB_USER=$FILESENDER_DB_USER
DATETIME=$(date --rfc-3339=seconds)

USER_COUNT=$(psql --host localhost --username $DB_USER --dbname $DB_NAME --single-line --tuples-only --command "SELECT COUNT(id) FROM authentications WHERE saml_user_identification_uid ~ '[0-9a-f]+-[0-9a-f]+-[0-9a-f]+-[0-9a-f]+-[0-9a-f]+'")
CURRENTLY_AVAILABLE_TRANSFERS=$(psql --host localhost --username $DB_USER --dbname $DB_NAME --single-line --tuples-only --command "SELECT COUNT(id) FROM transfers WHERE status='available'")
CREATED_TRANSFERS=$(psql --host localhost --username $DB_USER --dbname $DB_NAME --single-line --tuples-only --command "SELECT count(id) FROM statlogs WHERE event='transfer_available'")
FILES_UPLOADED=$(psql --host localhost --username $DB_USER --dbname $DB_NAME --single-line --tuples-only --command "SELECT count(id) FROM statlogs WHERE event='file_uploaded'")
FILES_DOWNLOADED=$(psql --host localhost --username $DB_USER --dbname $DB_NAME --single-line --tuples-only --command "SELECT count(id) FROM statlogs WHERE event='download_ended'")
TRANSFERED_UP=$(psql --host localhost --username $DB_USER --dbname $DB_NAME --single-line --tuples-only --command "SELECT sum(size) FROM statlogs WHERE event='upload_ended'")
TRANSFERED_DOWN=$(psql --host localhost --username $DB_USER --dbname $DB_NAME --single-line --tuples-only --command "SELECT sum(size) FROM statlogs WHERE event='download_ended'")
USED_SPACE=$(psql --host localhost --username $DB_USER --dbname $DB_NAME --single-line --tuples-only --command "SELECT size FROM statlogs WHERE event='global_storage_usage' ORDER BY id DESC limit 1")

echo $DATETIME,$USER_COUNT,$CURRENTLY_AVAILABLE_TRANSFERS,$CREATED_TRANSFERS,$FILES_UPLOADED,$FILES_DOWNLOADED,$TRANSFERED_UP,$TRANSFERED_DOWN,$USED_SPACE, >> ./stats/usage-stats-dkfz-filesender-weekly.csv
